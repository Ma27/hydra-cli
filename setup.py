from setuptools import setup, find_packages
import sys

assert sys.version_info >= (3, 6, 0), "hydra-cli requires Python 3.6+"

setup(
    name="hydra-cli",
    version="0.1.0",
    description="Query the Hydra CI API",
    author="Maximilian Bosch",
    author_email="maximilian@mbosch.me",
    license="MIT",
    packages=find_packages(),
    entry_points={"console_scripts": ["hydra-cli=hydra_cli:main"]},
    install_requires=["requests >=2.19.1,<3.0", "tabulate >=0.8.2,<1.0"],
    extras_require={"dev": ["flake8>=3.5,<3.6"]},
    test_suite='nose.collector',
    setup_requires=['nose >=1.3,<2.0', 'nose-cov >=1.5,<2.0']
)
