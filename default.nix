{ pkgs ? import <nixpkgs> { } }:

with pkgs;
with python3.pkgs;

buildPythonApplication {
  name = "hydra-cli";
  src = if lib.inNixShell then null else ./.;

  propagatedBuildInputs = [ requests tabulate ];
  checkInputs = [ nose nose-cov flake8 pylint autopep8 mypy ];

  shellHook = ''
    root_dir=$(git rev-parse --show-toplevel)
    alias dev-cli="${python}/bin/python $root_dir/hydra_cli/__init__.py"
    alias reformat="find {setup.py,hydra_cli,tests} -regex ".*\.py$" | xargs autopep8 --in-place --aggressive --aggressive"
    export PYTHONPATH="$PYTHONPATH:$root_dir"
  '';
}
