"""
Simple helper which deals provides utility to locally
cache files in ~/.cache.
"""
import os
from typing import Optional, Callable
from requests import Response


class Cache():
    """
    Simple cache utility which ensures that `~/.cache/hydra-cli` exists
    and ensures that cache entries exist.
    """

    def __init__(self, cache_dir: Optional[str] = None):
        default = "%s/.cache/hydra-cli" % os.environ['HOME']
        self.cache_dir = default if cache_dir is None else cache_dir

    def init_cache(self):
        """
        Initializes the specified cache directory in ~/.cache.
        """
        if not os.path.exists(self.cache_dir):
            os.mkdir(self.cache_dir)

    def ensure_cache_entry(
            self,
            cache_path: str,
            provider: Callable[[], Response],
            mode: int
    ) -> str:
        """
        Ensures that a given cache path exists and optionally invokes the `provider`
        which returns an iterable object (i.e. a response from `requests`) and its content
        is written to cache.

        As this is only used by the `reproduce-build` functionality, so no expiry date functionality
        is implemented as builds are immutable.
        """
        path = "%s/%s" % (self.cache_dir, cache_path)
        if not os.path.isfile(path):
            with open(path, 'wb+') as cache_entry:
                for chunk in provider():
                    cache_entry.write(chunk)
            os.chmod(path, mode)
        return path
