"""
CLI Application parses `argv` and decides what to do.
"""
import argparse
from typing import List
import sys
from hydra_cli.cache import Cache
from hydra_cli.api import transform_host_name
from hydra_cli.commands.project import ProjectParser
from hydra_cli.commands.reproduce_build import ReproduceBuildParser


class Application():
    """
    Initializes the CLI application. It parses the base command from `sys.argv`
    and passes the remaining args to the correct method (which processes one subcommand).
    """

    def __init__(self, cmd: str, args: List[str]):
        self.project_choices = [
            'name',
            'description',
            'displayname',
            'enabled',
            'hidden',
            'jobsets',
            'owner',
            'releases'
        ]

        parser = argparse.ArgumentParser(cmd, description="Query Hydra API")

        self.cache = Cache()
        self.cache.init_cache()

        subparsers = parser.add_subparsers(dest="command")
        subparsers.required = True

        project = ProjectParser(self.project_choices)
        project_parser = project.create_subcli(subparsers)

        reproduce_build = ReproduceBuildParser(self.cache)
        reproduce_build_parser = reproduce_build.create_subcli(subparsers)

        for subcmd in [project_parser, reproduce_build_parser]:
            subcmd.add_argument(
                '-H',
                '--host',
                help='The host for Hydra',
                default='https://hydra.nixos.org',
                type=transform_host_name
            )
            subcmd.add_argument(
                '-p',
                '--plain',
                help='Whether to use a plain table output easily parseable. Useful with scripts.',
                default=False,
                action='store_true'
            )
            subcmd.add_argument(
                '-d',
                '--dev',
                help='Development mode: show full traces in case of errors thrown',
                default=False,
                action='store_true'
            )

        # only retrieve top-level command
        argv = parser.parse_args(args)

        try:
            argv.func(argv)
        except Exception as err:
            if not argv.dev:
                print("Aborted program: %s" % err, file=sys.stderr)
                exit(1)
            else:
                raise err
