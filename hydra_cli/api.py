"""
Utilities to communicate with the Hydra API.
"""

from typing import List, Dict, Any
import re
from requests import Response, get
from hydra_cli.tables import get_desired_fields


class ApiError(Exception):
    """
    Custom exception for errors from the Hydra REST API.
    """


def transform_host_name(host: str) -> str:
    """
    Transforms the host name given by `-H` to a hostname that always has a protocol defined.
    `requests` requires a protocol in the URL, so we add `https://` by default unless
    something else was specified.
    """
    return host if re.search('^http(?:s)://', host) is not None else 'https://%s' % host


def ensure_is_hydra(response: Response) -> bool:
    """
    Heuristic to ensure that a given response actually came from a Hydra instance.
    """
    return 'hydra_session' in response.cookies


def perform_api_call_json(
        host: str,
        property_path: str,
        fields: List[str],
        is_single: bool,
) -> Dict[str, List[Any]]:
    """
    Performs an API call to a given Hydra server, ensures a JSON response with
    a configurable set of fields is returned and handles possible errors.
    """
    json = perform_api_call(host, property_path, {'Accept': 'application/json'}).json()

    return get_desired_fields(json if not is_single else [json], fields)


# pylint: disable=W0102
def perform_api_call(
        host: str,
        property_path: str,
        headers: Dict[str, str] = {}
) -> Response:
    """
    Performs a raw API call to a Hydra, ensures that the request actually comes from a Hydra
    and returns the response.
    """
    url = "%s/%s" % (host, property_path)
    response = get(url, headers=headers)
    if response.status_code != 200:
        raise ApiError("Cannot gather %s from %s" % (property_path, host))
    if not ensure_is_hydra(response):
        raise Exception("%s does not appear to be a Hydra!" % host)

    return response
