"""
Collection of general utility for table rendering and transformation.
"""
from typing import List, Dict, Any, Tuple
from tabulate import tabulate


def get_desired_fields(json: List[Dict[str, Any]], fields: List[str]) -> Dict[str, List[Any]]:
    """
    Fetches desired fields from a list of objects describing a Hydra
    object (e.g. a jobset or a project).

    The basic transformation looks like this:
    [
        {"name":"value", "enabled":1, "description": "Foobar"},
        /* and so on */
    ]
    -> get_desired_fields(..., ['name']) ->
    {
        "name": ["value", ...]
    }
    """
    return {f: list(map(lambda x: x[f], json)) for f in fields}


def render_table(plain: bool, table: Dict[str, List[Any]]) -> str:
    """
    Renders the dict by creating a table using `tabulate` and
    transforming object names to readable entities using `.title()`.
    """
    keys = table.keys()
    style = "plain" if plain else "simple"
    headers = [] if plain else list(map(lambda x: x.title(), keys))

    return tabulate(
        dict(map(lambda x: flatten_arrays(x, table[x]), keys)),
        headers=headers,
        tablefmt=style
    )


def flatten_arrays(key: str, data: List[Any]) -> Tuple[str, List[Any]]:
    """
    Flattens arrays in a row, so they're rendered comma-separated rather
    than in the classical array form `['foo', 'bar', 'baz']`.
    """
    return (key, [','.join(x) if isinstance(x, list) else x for x in data])
