"""
CLI parser and handler for the `reproduce-build` sub-command
"""

from argparse import _SubParsersAction, ArgumentParser, Namespace
import os
from urllib.parse import urlparse
from hydra_cli.cache import Cache
from hydra_cli.api import perform_api_call


class ReproduceBuildParser():
    """
    Helper class for the `reproduce-build` sub-command.
    """
    def __init__(self, cache: Cache) -> None:
        self.cache = cache

    def create_subcli(self, subparse: _SubParsersAction) -> ArgumentParser:
        """
        Generate arguments for the `reproduce-build` subcommand.
        """
        reproduce_build = subparse.add_parser('reproduce-build')

        reproduce_build.add_argument('build_nr', help='The build ID to reproduce locally', type=int)

        reproduce_build.set_defaults(func=self.run_command)

        return reproduce_build

    def run_command(self, argv: Namespace):
        """
        Calls the reproduce build functionality from Hydra for a given build ID.
        """

        host = '{uri.netloc}'.format(uri=urlparse(argv.host))
        script_path = 'reproduce-build-%s-%d' % (host, argv.build_nr)

        full_path = self.cache.ensure_cache_entry(
            script_path,
            lambda: perform_api_call(argv.host, '/build/%d/reproduce' % argv.build_nr),
            0o775
        )

        # https://bugs.python.org/issue421250
        os.execvp(full_path, [script_path])
