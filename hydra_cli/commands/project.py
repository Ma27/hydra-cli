"""
CLI parser and handler for the `project` sub-command
"""
from typing import List
from argparse import _SubParsersAction, ArgumentParser, Namespace
from hydra_cli.api import perform_api_call_json
from hydra_cli.tables import render_table


class ProjectParser():
    """
    Helper class for the `project` subcommand.
    """
    def __init__(self, project_choices: List[str]) -> None:
        self.project_choices = project_choices

    def create_subcli(self, subparse: _SubParsersAction) -> ArgumentParser:
        """
        Generate arguments for the `project` subcommand.
        """
        project = subparse.add_parser('project')
        project.add_argument(
            '-f',
            '--fields',
            help='Which fields to display',
            default=['name'],
            choices=self.project_choices,
            nargs='*'
        )
        project.add_argument(
            '--verbose',
            help='Verbose view: show all properties',
            action='store_true'
        )

        project.add_argument(
            'show',
            help='Display information about a single project',
            nargs='?',
            default=None
        )
        project.set_defaults(func=self.run_command)

        return project

    def run_command(self, argv: Namespace):
        """
        Show available build projects from Hydra (mainly list projects or show details).
        """

        fields = argv.fields
        if argv.verbose:
            fields = self.project_choices

        is_single = argv.show is not None
        path = "/project/%s" % argv.show if is_single else "/"

        print(render_table(
            argv.plain,
            perform_api_call_json(argv.host, path, fields, is_single)
        ))
