"""
Entrypoint for `hydra-cli`: `main` can be called with setuptools or directly via
`python hydra_cli/__init__.py`
"""
import sys
from hydra_cli.cli import Application


def main() -> None:
    """
    Main entrypoint for `hydra-cli`: passes argv to the CLI app.
    """
    try:
        command = sys.argv[0]
        args = sys.argv[1:]

        Application(command, args)
    except KeyboardInterrupt:
        pass


if __name__ == "__main__":
    main()
