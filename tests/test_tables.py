from unittest import TestCase
from hydra_cli.tables import get_desired_fields, render_table
from textwrap import dedent


class TablesTest(TestCase):
    def test_get_desired_fields(self):
        data = [
            {
                "name": "Demo",
                "enabled": 1,
                "description": "Demo project"
            },
            {
                "name": "Example",
                "enabled": 0,
                "description": "Something else"
            }
        ]

        assert get_desired_fields(data, ["name"]) == {
            "name": ["Demo", "Example"],
        }

    def test_render_table(self):
        expected = '''
          Name
          -------
          Demo
          Example
        '''
        assert render_table(False, {"name": ["Demo", "Example"]}) == dedent(expected).strip()

    def test_render_table_with_plain(self):
        expected = '''
          Demo     Foo
          Example  Barz
        '''
        result = render_table(True, {"name": ["Demo", "Example"], "test": ["Foo", "Barz"]})
        assert result == dedent(expected).strip()

    def test_render_table_with_comma(self):
        expected = '''
          Demo     Foo,Bar
          Example  Barz,Bar
        '''
        result = render_table(
            True,
            {"name": ["Demo", "Example"], "test": [["Foo", "Bar"], ["Barz", "Bar"]]}
        )
        assert result == dedent(expected).strip()
