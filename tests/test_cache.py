from unittest import TestCase
from hydra_cli.cache import Cache
import os
import shutil


class CacheTest(TestCase):
    def test_set_default_path(self):
        cache = Cache()
        assert cache.cache_dir.endswith('/.cache/hydra-cli')

    def test_init_cache_dir(self):
        current = os.path.dirname(os.path.realpath(__file__)) + '/.tmp'
        cache = Cache(current)
        cache.init_cache()

        self.__assert_and_clean(lambda: os.path.isdir(current), current)

    def test_ensure_cache(self):
        def demo_provider():
            yield b"foo"
            yield b"bar\n"
            yield b"baz"

        def check_file():
            path = current + '/test'
            with open(path, 'r+') as f:
                return f.readline() == 'foobar\n' and oct(os.lstat(path).st_mode) == oct(0o100644)

        current = os.path.dirname(os.path.realpath(__file__)) + '/.tmp'
        cache = Cache(current)
        cache.init_cache()

        cache.ensure_cache_entry('test', demo_provider, 0o644)

        self.__assert_and_clean(check_file, current)

    def __assert_and_clean(self, assertion, path_to_cleanup):
        try:
            assert assertion()
        finally:
            shutil.rmtree(path_to_cleanup)
