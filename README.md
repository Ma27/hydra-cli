# hydra-cli

[![pipeline status](https://gitlab.com/Ma27/hydra-cli/badges/master/pipeline.svg)](https://gitlab.com/Ma27/hydra-cli/commits/master)
[![coverage report](https://gitlab.com/Ma27/hydra-cli/badges/master/coverage.svg)](https://gitlab.com/Ma27/hydra-cli/commits/master)

Query [Hydra](https://nixos.org/hydra/) information from the CLI.

## Usage

To be documented.

Please keep in mind that this project is at a very early stage currently and can't be
used as it lacks most features planned.

## Hacking

It's recommended to open a
[`nix-shell`](https://nixos.org/nixos/nix-pills/developing-with-nix-shell.html) which contains
all runtime dependencies and utilities such as `flake8`.
